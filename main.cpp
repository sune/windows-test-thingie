
#include <QApplication>
#include <QTest>


class WindowsTestThingie : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testShowWindow() {
	if (qgetenv("KDECI_CANNOT_CREATE_WINDOWS") == "1") {
	    QSKIP("KDE CI can't create a window on this platform, skipping some gui tests");
	}
            QWidget w;
            w.show();

            QVERIFY(QTest::qWaitForWindowExposed(&w));
        }
};

QTEST_MAIN(WindowsTestThingie);
#include "main.moc"
