
#include <QApplication>
#include <QTest>


class WindowsTestThingie : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testShowWindow() {
            QWidget w;
            w.show();
            QVERIFY(QTest::qWaitForWindowExposed(&w));
        }
};

QTEST_MAIN(WindowsTestThingie);
#include "main.moc"
